import AppFlashMessage from 'components/app-flash-message/AppFlashMessage';
import AppNavigation from 'navigation/AppNavigation';
import {StyleSheet} from 'react-native';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import rootStore from 'redux-manager/rootStore';

const App = () => {
  return (
    <Provider store={rootStore}>
      <GestureHandlerRootView style={styles.container}>
        <AppNavigation />
        <AppFlashMessage />
      </GestureHandlerRootView>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
