import {colors} from 'assets';
import React, {useEffect} from 'react';
import FlashMessage, {
  MessageType,
  showMessage,
} from 'react-native-flash-message';
import {useDispatch, useSelector} from 'react-redux';
import allActions from 'redux-manager/allActions';
import {RootState} from 'redux-manager/rootReducer';

interface MessageTypeItem {
  label: MessageType;
  bgColor: string;
}

const messageType: Record<string, MessageTypeItem> = {
  error: {
    label: 'danger',
    bgColor: colors.error,
  },
  success: {
    label: 'success',
    bgColor: colors.success,
  },
  info: {
    label: 'info',
    bgColor: colors.info,
  },
  warning: {
    label: 'warning',
    bgColor: colors.warning,
  },
};

const AppFlashMessage = () => {
  const {currentMessage} = useSelector((state: RootState) => state.common);
  const dispatch = useDispatch();
  useEffect(() => {
    if (currentMessage) {
      showMessage({
        message: currentMessage?.message,
        type: messageType[currentMessage?.type]?.label,
        backgroundColor: messageType[currentMessage?.type]?.bgColor,
        description: currentMessage?.description,
        onHide: () => {
          dispatch(allActions.commonActions.clearFlashMessage());
        },
        duration: currentMessage?.visibilityTime || 1200,
      });
    }
  }, [currentMessage, dispatch]);
  return <FlashMessage position="top" />;
};

export default AppFlashMessage;
