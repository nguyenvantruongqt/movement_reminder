import { combineReducers } from 'redux';
import common from './common/index';

const appReducer = combineReducers({
  common,
});

const rootReducer = (state: any, action: any) => appReducer(state, action);

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;