import { applyMiddleware, createStore } from 'redux';
import rootReducer from './rootReducer';

const middlewares = [];

if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

const rootStore = createStore(rootReducer, applyMiddleware(...middlewares));
export default rootStore;
