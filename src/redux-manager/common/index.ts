import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { AppFlashMessage, CommonState } from './types';

export const initialState: CommonState = {
  currentMessage: undefined
};

const common = createSlice({
  name: 'common',
  initialState,
  reducers: {
    showFlashMessage(state, action: PayloadAction<AppFlashMessage>) {
      state.currentMessage = action.payload;
    },
    clearFlashMessage(state) {
      state.currentMessage = undefined;
    },
  }
});

export const { actions: commonActions } = common;

export default common.reducer;