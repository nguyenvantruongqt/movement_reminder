export interface AppFlashMessage {
  message: string;
  type: 'error' | 'success' | 'info' | 'warning';
  autoHide?: boolean;
  visibilityTime?: number;
  description?: string;
}

export interface CommonState {
  currentMessage?: AppFlashMessage;
}
