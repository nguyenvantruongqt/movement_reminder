export { default as HomeActiveIcon } from 'assets/icons/home_active.svg';
export { default as HomeInactiveIcon } from 'assets/icons/home_inactive.svg';

const colors = {
  PRIMARY: '#F57138',
  text1: '#383B3F',
  text2: '#4D4D4D',
  text3: '#888A8D',
  text4: '#B7B8BA',
  white: 'white',
  black: 'black',
  red: 'red',
  c9F9894: '#9F9894',
  c5F534D: '#5F534D',
  error: '#d9534f',
  info: '#5bc0de',
  warning: '#f0ad4e',
  success: '#05C0DE',
};

const icons = {
  homeIcon: require('./icons/home.png'),
  settingIcon: require('./icons/setting.png'),
};

export {icons, colors};