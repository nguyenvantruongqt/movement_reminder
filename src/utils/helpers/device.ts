import { Dimensions, Platform } from 'react-native';

const device = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const isIos = Platform.OS === 'ios';

export { device, isIos };
