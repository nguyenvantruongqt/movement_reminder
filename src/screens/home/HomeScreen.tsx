import {Pressable, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useDispatch} from 'react-redux';
import allActions from 'redux-manager/allActions';

const HomeScreen = () => {
  const insets = useSafeAreaInsets();
  const dispatch = useDispatch();
  return (
    <View
      style={{
        marginTop: insets.top,
      }}>
      <Text>HomeScreen</Text>
      <TouchableOpacity
        onPress={() => {
          dispatch(
            allActions.commonActions.showFlashMessage({
              message: 'TEST',
              type: 'success',
            }),
          );
        }}>
        <Text>TEST</Text>
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;
