import {Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const SettingScreen = () => {
  const insets = useSafeAreaInsets();

  return (
    <View
      style={{
        marginTop: insets.top,
      }}>
      <Text>SettingScreen</Text>
    </View>
  );
};

export default SettingScreen;
