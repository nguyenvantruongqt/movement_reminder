export enum SCREEN_NAME {
  BottomTabNavigation = 'BottomTabNavigation',
  HomeTab = 'Home',
  SettingTab = 'Setting',
}

export type StackParamList = {
  [SCREEN_NAME.BottomTabNavigation]: undefined;
  [SCREEN_NAME.HomeTab]: undefined;
  [SCREEN_NAME.SettingTab]: undefined;
}