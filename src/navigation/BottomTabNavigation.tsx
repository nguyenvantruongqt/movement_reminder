import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {colors, icons} from 'assets';
import {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Image,
  StyleSheet,
  Text,
  View,
  useWindowDimensions,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import HomeScreen from 'screens/home/HomeScreen';
import SettingScreen from 'screens/setting/SettingScreen';
import {device, isIos} from 'utils/helpers/device';
import {SCREEN_NAME} from './ScreenProps';

const Tab = createBottomTabNavigator();

interface TProps {
  children: React.ReactNode;
}

const TabBorder = ({children}: TProps) => {
  return <View>{children}</View>;
};

const BottomTabNavigation = () => {
  const insets = useSafeAreaInsets();
  const tabBarHeight = 65 + insets.bottom / 2;
  const window = useWindowDimensions();
  const [topBarWidth, setTopBarWidth] = useState(window.width / 2);
  const tabOffsetValue = useRef(new Animated.Value(-topBarWidth / 2)).current;

  useEffect(() => {
    setTopBarWidth(window.width / 2);
  }, [window.width]);

  const renderItem = (isFocused: boolean, screenName: string) => {
    const tabColor = isFocused ? colors.PRIMARY : colors.text4;

    const getIcon = () => {
      switch (screenName) {
      case SCREEN_NAME.HomeTab:
        return (
          <Image
            source={icons.homeIcon}
            style={styles.icon}
            tintColor={isFocused ? colors.PRIMARY : colors.c5F534D}
          />
        );

      case SCREEN_NAME.SettingTab:
        return (
          <Image
            source={icons.settingIcon}
            style={styles.icon}
            tintColor={isFocused ? colors.PRIMARY : colors.c5F534D}
          />
        );
      default:
        break;
      }
    };

    return (
      <View
        style={[
          styles.iconContainer,
          {
            width: topBarWidth,
          },
        ]}>
        {getIcon()}
        <Text style={[styles.itemText, {color: tabColor}]}>{screenName}</Text>
      </View>
    );
  };

  return (
    <>
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarShowLabel: false,
          tabBarStyle: {
            paddingVertical: insets.bottom / 3,
            height: tabBarHeight,
            ...styles.tabNavigator,
          },
          tabBarHideOnKeyboard: !isIos,
        }}>
        <Tab.Screen
          name={SCREEN_NAME.HomeTab}
          component={HomeScreen}
          options={{
            tabBarIcon: ({focused}) => (
              <TabBorder {...{focused}}>
                {renderItem(focused, SCREEN_NAME.HomeTab)}
              </TabBorder>
            ),
          }}
          listeners={() => ({
            tabPress: () => {
              Animated.spring(tabOffsetValue, {
                toValue: -topBarWidth / 2,
                useNativeDriver: true,
              }).start();
            },
          })}
        />
        <Tab.Screen
          name={SCREEN_NAME.SettingTab}
          component={SettingScreen}
          options={{
            tabBarIcon: ({focused}) => (
              <TabBorder {...{focused}}>
                {renderItem(focused, SCREEN_NAME.SettingTab)}
              </TabBorder>
            ),
          }}
          listeners={() => ({
            tabPress: () => {
              Animated.spring(tabOffsetValue, {
                toValue: topBarWidth / 2,
                useNativeDriver: true,
              }).start();
            },
          })}
        />
      </Tab.Navigator>
      <Animated.View
        style={[
          styles.animatedView,
          {
            width: topBarWidth,
            bottom: tabBarHeight - 2,
            transform: [{translateX: tabOffsetValue}],
          },
        ]}
      />
    </>
  );
};

export default BottomTabNavigation;

const styles = StyleSheet.create({
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  borderTop: {
    width: device.width,
    height: 1,
    position: 'absolute',

    elevation: 99,
    backgroundColor: colors.red,
  },
  tabNavigator: {
    backgroundColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: -5,
    },
    shadowOpacity: 0.05,
    shadowRadius: 2,
    elevation: 3,
  },
  itemText: {
    fontSize: 13,
    marginTop: 5,
  },
  animatedView: {
    height: 2,
    alignSelf: 'center',
    backgroundColor: colors.PRIMARY,
    position: 'absolute',
    elevation: 999,
  }
});
